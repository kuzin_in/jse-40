package ru.kuzin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.kuzin.tm.api.endpoint.IAuthEndpoint;
import ru.kuzin.tm.api.endpoint.ITaskEndpoint;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.dto.request.*;
import ru.kuzin.tm.dto.response.*;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.marker.SoapCategory;
import ru.kuzin.tm.model.Task;
import ru.kuzin.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String token;

    @Nullable
    private Task task;

    @Before
    public void initTest() throws Exception {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        token = loginResponse.getToken();
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token);
        createRequest.setName("Test Task 1");
        createRequest.setDescription("Test Description 1");
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        Assert.assertNotNull(createResponse);
        task = createResponse.getTask();
    }

    @After
    public void initEndTest() throws Exception {
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testChangeTaskStatusById() throws Exception {
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        @NotNull final Status newStatus = Status.IN_PROGRESS;

        @NotNull final TaskChangeStatusByIdRequest taskChangeStatusByIdRequest = new TaskChangeStatusByIdRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest)
        );
        taskChangeStatusByIdRequest.setId("");
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest)
        );
        taskChangeStatusByIdRequest.setStatus(null);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeTaskStatusById(taskChangeStatusByIdRequest)
        );
        taskChangeStatusByIdRequest.setId(taskId);
        taskChangeStatusByIdRequest.setStatus(newStatus);
        @NotNull final TaskChangeStatusByIdResponse changeStatusByIdResponse = taskEndpoint.changeTaskStatusById(
                taskChangeStatusByIdRequest
        );
        @Nullable final Task actualTask = changeStatusByIdResponse.getTask();
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(Status.IN_PROGRESS, actualTask.getStatus());
    }

    @Test
    public void testClearTask() throws Exception {
        @NotNull final TaskListRequest listRequest = new TaskListRequest(token);
        @Nullable List<Task> tasks = taskEndpoint.listTask(listRequest).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);

        taskEndpoint.clearTask(new TaskClearRequest(token));
        tasks = taskEndpoint.listTask(listRequest).getTasks();
        Assert.assertNull(tasks);
    }

    @Test
    public void testCreateTask() throws Exception {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(createRequest)
        );
        createRequest.setName("");
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(createRequest)
        );
        createRequest.setName("Test Task 2");
        createRequest.setDescription("Test Description 2");
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        Assert.assertNotNull(createResponse);
        @Nullable Task actualTask = createResponse.getTask();
        Assert.assertNotNull(actualTask);
    }

    @Test
    public void testShowTaskById() throws Exception {
        Assert.assertNotNull(task);
        @NotNull final String id = task.getId();
        @NotNull final TaskShowByIdRequest taskShowByIdRequest = new TaskShowByIdRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.showTaskById(taskShowByIdRequest)
        );
        taskShowByIdRequest.setId("");
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.showTaskById(taskShowByIdRequest)
        );
        taskShowByIdRequest.setId("otherId");
        Assert.assertNull(taskEndpoint.showTaskById(taskShowByIdRequest).getTask());

        taskShowByIdRequest.setId(id);
        @NotNull final TaskShowByIdResponse taskShowByIdResponse = taskEndpoint.showTaskById(taskShowByIdRequest);
        Assert.assertNotNull(taskShowByIdResponse);
        @Nullable final Task actualTask = taskShowByIdResponse.getTask();
        Assert.assertNotNull(actualTask);
        Assert.assertEquals("Test Task 1", actualTask.getName());
    }

    @Test
    public void testShowTaskList() throws Exception {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token);
        createRequest.setName("Task 2");
        createRequest.setDescription("Description 2");
        taskEndpoint.createTask(createRequest);
        createRequest.setName("Task 3");
        createRequest.setDescription("Description 3");
        taskEndpoint.createTask(createRequest);

        @NotNull final TaskListRequest taskListRequest = new TaskListRequest(token);
        @Nullable final List<Task> tasks = taskEndpoint.listTask(taskListRequest).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(3, tasks.size());
    }

    @Test
    public void testRemoveTaskById() throws Exception {
        @NotNull final TaskListRequest taskListRequest = new TaskListRequest(token);
        @Nullable List<Task> tasks = taskEndpoint.listTask(taskListRequest).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        @NotNull final String id = tasks.get(0).getId();

        @NotNull final TaskRemoveByIdRequest taskRemoveByIdRequest = new TaskRemoveByIdRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(taskRemoveByIdRequest)
        );
        taskRemoveByIdRequest.setId("");
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(taskRemoveByIdRequest)
        );
        taskRemoveByIdRequest.setId("otherId");
        Assert.assertNull(taskEndpoint.removeTaskById(taskRemoveByIdRequest).getTask());

        taskRemoveByIdRequest.setId(id);
        @NotNull final TaskRemoveByIdResponse taskRemoveByIdResponse = taskEndpoint.removeTaskById(taskRemoveByIdRequest);
        Assert.assertNotNull(taskRemoveByIdResponse);
        tasks = taskEndpoint.listTask(taskListRequest).getTasks();
        Assert.assertNull(tasks);
    }

    @Test
    public void testUpdateTaskById() throws Exception {
        Assert.assertNotNull(task);
        @Nullable String id = task.getId();

        @NotNull final TaskUpdateByIdRequest taskUpdateByIdRequest = new TaskUpdateByIdRequest(token);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId(id);
        taskUpdateByIdRequest.setName("New task name");
        taskUpdateByIdRequest.setDescription("New description");
        @NotNull final TaskUpdateByIdResponse taskUpdateByIdResponse = taskEndpoint.updateTaskById(
                taskUpdateByIdRequest
        );
        Assert.assertNotNull(taskUpdateByIdResponse);
        @Nullable Task actualTask = taskUpdateByIdResponse.getTask();
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(id, actualTask.getId());
        Assert.assertEquals("New task name", actualTask.getName());
        Assert.assertEquals("New description", actualTask.getDescription());

        taskUpdateByIdRequest.setId("");
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId("otherId");
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId(id);
        taskUpdateByIdRequest.setName("");
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
        taskUpdateByIdRequest.setId(null);
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(taskUpdateByIdRequest)
        );
    }

}