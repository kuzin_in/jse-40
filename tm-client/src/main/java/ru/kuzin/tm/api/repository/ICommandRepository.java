package ru.kuzin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(String argument);

    @Nullable
    AbstractCommand getCommandByName(String name);

    @NotNull
    Iterable<AbstractCommand> getCommandsWithArgument();

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}