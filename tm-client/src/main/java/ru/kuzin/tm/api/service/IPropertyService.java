package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLogs();

    @NotNull
    String getApplicationName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

    String getPort();

    @NotNull
    String getHost();

    @NotNull
    String getAdminLogin();

    @NotNull
    String getAdminPassword();

}