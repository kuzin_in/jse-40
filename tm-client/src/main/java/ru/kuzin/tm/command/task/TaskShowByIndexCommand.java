package ru.kuzin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.request.TaskShowByIndexRequest;
import ru.kuzin.tm.model.Task;
import ru.kuzin.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-show-by-index";

    @NotNull
    private static final String DESCRIPTION = "Display task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK BY INDEX");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final Task task = getTaskEndpoint().showTaskByIndex(request).getTask();
        showTask(task);
    }

}