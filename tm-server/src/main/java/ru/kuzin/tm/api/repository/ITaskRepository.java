package ru.kuzin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, name, created, description, user_id, status, project_id)" +
            " VALUES (#{task.id}, #{task.name}, #{task.created}, #{task.description}, #{userId}, #{task.status}, #{task.projectId})")
    void add(@NotNull @Param("userId") String userId, @NotNull @Param("task") Task task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT id, name, created, description, user_id, status, project_id FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT id, name, created, description, user_id, status, project_id FROM tm_task WHERE user_id = #{userId}" +
            " ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAllOrderName(@NotNull @Param("userId") String userId);

    @Select("SELECT id, name, created, description, user_id, status, project_id FROM tm_task WHERE user_id = #{userId}" +
            " ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAllOrderCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT id, name, created, description, user_id, status, project_id FROM tm_task WHERE user_id = #{userId}" +
            " ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAllOrderStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_task WHERE user_id = #{userId} AND id = #{id})")
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT id, name, created, description, user_id, status, project_id FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable Task findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT id, name, created, description, user_id, status, project_id FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable Task findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT count(id) FROM tm_task WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{task.id}")
    void remove(@NotNull @Param("userId") String userId, @NotNull @Param("task") Task task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Update("UPDATE tm_task SET name = #{task.name}, created = #{task.created}, description = #{task.description}," +
            " user_id = #{task.userId}, status = #{task.status}, project_id = #{task.projectId} WHERE id = #{task.id}")
    void update(@NotNull @Param("task") Task task);

    @Select("SELECT id, name, created, description, user_id, status, project_id FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @NotNull List<Task> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

}